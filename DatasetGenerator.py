import os
import cv2
import random
import numpy as np
import pickle
import Util

IMAGE_SIZE = 80


class DatasetGenerator:
    """
    Dataset generator
    """

    def __init__(self, path, files):
        self.path = path
        self.files = list(files)
        self.trainingData = list()
        self.features = list()
        self.labels = list()

    def load(self):
        """
        Loads training data
        """
        for file in self.files:
            path = os.path.join(self.path, file)
            class_num = self.files.index(file)
            for img in os.listdir(path):
                if not Util.checkExtension(img):
                    continue
                img_array = cv2.imread(os.path.join(path, img), cv2.IMREAD_GRAYSCALE)
                new_array = cv2.resize(img_array, (IMAGE_SIZE, IMAGE_SIZE))
                self.trainingData.append([new_array, class_num])

    def get_data(self):
        """
        Returns data for training
        """
        random.shuffle(self.trainingData)

        for features, label in self.trainingData:
            self.features.append(features)
            self.labels.append(label)

        self.features = np.array(self.features).reshape(-1, IMAGE_SIZE, IMAGE_SIZE, 1)
        return self.trainingData

    def save_data(self):
        """
        Saves data, features and labels
        """
        pickle_out = open("features.pickle", "wb")
        pickle.dump(self.features, pickle_out)
        pickle_out.close()

        pickle_out = open("labels.pickle", "wb")
        pickle.dump(self.labels, pickle_out)
        pickle_out.close()
        print("Data saved!")
