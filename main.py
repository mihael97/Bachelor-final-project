import tensorflow as tf
import ConvolutionalNetworkTester
import DatasetGenerator
import ConvolutionalNetwork
import os
from datetime import datetime
import time
import sys
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="System for traffic sign recognition",
        epilog="System is created as bachelor thesis. Owned by Mihael Macuka. For any help or questions, "
               "contact owner with mail at \"mihaelmacuka2@gmail.com\"")
    parser.add_argument('train_directory', nargs=1, help='Location of train directory')
    parser.add_argument('test_directory', nargs=1, help='Location of test directory')
    parser.add_argument('classes', nargs='*',
                        help='recognition classes, directories inside test and train directory with samples')
    if len(sys.argv) < 3:
        parser.print_help(sys.stdout)
        sys.exit(1)
    args = vars(parser.parse_args())
    DATADIR = args["train_directory"][0]
    DATADIR_TEST = args["test_directory"][0]
    training_data = []
    CATEGORIES = args["classes"]

    error = False
    for category in CATEGORIES:
        test = os.path.join(DATADIR, category)
        train = os.path.join(DATADIR_TEST, category)
        if not os.path.exists(test):
            print("Path {} doesn't exist!".format(test))
            error = True
        if not os.path.exists(train):
            print("Path {} doesn't exist!".format(train))
            error = True
    if error is True:
        print("Exit because some directory doesn't exist")
        exit(1)
    model_name = "Tensorflow-traffic-sign-detector-{}".format(int(time.time()))
    print(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    dataset = DatasetGenerator.DatasetGenerator(DATADIR, CATEGORIES)
    dataset.load()
    dataset.get_data()
    dataset.save_data()
    network = ConvolutionalNetwork.ConvolutionalNetwork(model_name)
    network.train()
    print(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    network.save()
    model = tf.keras.models.load_model("model/{}.model".format(model_name))
    # model = tf.keras.models.load_model("model/Tensorflow-traffic-sign-detector-1558774518.model")
    model.summary()
    if os.path.exists("izlaz/Izlaz.txt"):
        os.remove("izlaz/Izlaz.txt")
    f = open("izlaz/Izlaz.txt", "w")
    f.write("Size is " + str(len(CATEGORIES)) + "\n")
    for category in CATEGORIES:
        f.write(category + " ")
    f.write("\n")
    f.close()
    for category in CATEGORIES:
        ConvolutionalNetworkTester.test(model, os.path.join(DATADIR_TEST, category),
                                        CATEGORIES, category)
