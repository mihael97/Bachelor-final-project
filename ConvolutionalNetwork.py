import tensorflow
from keras.utils import np_utils
from sklearn.preprocessing import LabelEncoder
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D
import pickle
from tensorflow.keras.callbacks import TensorBoard


class ConvolutionalNetwork:
    """
    Convolutional network
    """

    def normalize(self):
        """
        Normalizes features and labels
        """
        self.features = self.features / 255
        self.labels = self.convert_labels()

    def convert_labels(self):
        """
        Converts labels into category
        :return: labels as categories
        """
        encoder = LabelEncoder()
        encoder.fit(self.labels)
        y = encoder.transform(self.labels)
        y = np_utils.to_categorical(y)
        return y

    def __init__(self, model_name):
        self.features = pickle.load(open("features.pickle", "rb"))
        self.labels = pickle.load(open("labels.pickle", "rb"))
        self.model = Sequential()
        self.normalize()
        self.size = len(self.labels[1])
        self.model_name = model_name
        self.tensor_board = TensorBoard(log_dir="logs/{}".format(self.model_name))

    def train(self):
        """
        Trains neural network
        """
        gpu_options = tensorflow.GPUOptions(per_process_gpu_memory_fraction=0.33)
        sess = tensorflow.Session(config=tensorflow.ConfigProto(gpu_options=gpu_options))
        self.model.add(Conv2D(256, (3, 3), input_shape=self.features.shape[1:]))
        self.model.add(Activation('relu'))
        self.model.add(MaxPooling2D(pool_size=(2, 2)))

        self.model.add(Conv2D(256, (3, 3)))
        self.model.add(Activation('relu'))
        self.model.add(MaxPooling2D(pool_size=(2, 2)))

        self.model.add(Flatten())

        self.model.add(Dense(64))
        self.model.add(Activation('relu'))

        self.model.add(Dense(self.size))
        self.model.add(Activation('softmax'))

        self.model.compile(loss='categorical_crossentropy',
                           optimizer='adam',
                           metrics=['accuracy'])
        self.model.fit(self.features, self.labels, batch_size=32, epochs=20, validation_split=0.1,
                       callbacks=[self.tensor_board])

    def save(self):
        """
        Saves trained model
        """
        self.model.save("model/{}.model".format(self.model_name))
        print("Model saved!")
