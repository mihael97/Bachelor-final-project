import Util
import os


def get_result(prediction, categories):
    """
    Connects prediction with category
    :param prediction:  prediction
    :param categories:  categories
    :return: correct category or None
    """
    prediction = prediction[0]
    index = 0
    for i in prediction:
        value = int(round(i))
        if value >= 1:
            return categories[index]
        index += 1
    return None


def result(prediction):
    prediction = prediction[0]
    print(prediction)
    for i in prediction:
        if i >= 0.5:
            return True
    return False


def test(model, path, categories, result):
    """
    Test all images
    :param model: model
    :param path: path with images
    :param categories: categories
    :param result:  correct result
    """
    total = 0
    positives = 0
    f = open("izlaz/Izlaz.txt", "a")
    for file in os.listdir(path):
        if not Util.checkExtension(file):
            continue

        total += 1
        filepath = os.path.join(path, file)
        image = Util.loadImageInGrayscale(filepath)

        if image is None:
            continue

        prediction = model.predict(image)
        prediction_result = get_result(prediction, categories)
        if prediction_result == result:
            positives += 1
        else:
            print(prediction_result)
            f.write("{}\n".format(prediction_result))
    print("Path: ", str(path), " ,total: ", str(total), " , positives: ", str(positives), ", efficiency: ", str(
        positives / total))
    f.write("Path: {}, total: {}, positives: {}, efficiency: {}\n".format(path, total, positives, positives / total))
    f.close()


def test_image(model, path):
    """
    Test one image
    :param model: model
    :param path:  image path
    :return: prediction
    """
    image = Util.loadImageInGrayscale(path)
    if image is None:
        return False
    return result(model.predict(image))
