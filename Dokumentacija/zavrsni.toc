\babel@toc {croatian}{}
\contentsline {chapter}{\numberline {1}Uvod}{1}
\contentsline {section}{\numberline {1.1}Primjena sustava za raspoznavanje prometnih znakova}{1}
\contentsline {chapter}{\numberline {2}Strojno u\IeC {\v c}enje i neuronske mre\IeC {\v z}e}{2}
\contentsline {section}{\numberline {2.1}Nedostaci strojnog u\IeC {\v c}enja}{2}
\contentsline {subsection}{\numberline {2.1.1}Unakrsna provjera}{3}
\contentsline {section}{\numberline {2.2}Neuronske mre\IeC {\v z}e}{3}
\contentsline {subsection}{\numberline {2.2.1}Arhitektura neuronske mre\IeC {\v z}e}{3}
\contentsline {subsection}{\numberline {2.2.2}Analiza unutar neurona}{4}
\contentsline {subsection}{\numberline {2.2.3}U\IeC {\v c}enje neuronske mre\IeC {\v z}e}{5}
\contentsline {chapter}{\numberline {3}Implementacija sustava}{7}
\contentsline {section}{\numberline {3.1}Implementacija pomo\IeC {\'c}u transformacija i analize boje}{7}
\contentsline {subsection}{\numberline {3.1.1}Houghova transformacija i njezina primjena}{7}
\contentsline {subsubsection}{Houghova transformacija za pravac}{8}
\contentsline {subsection}{\numberline {3.1.2}Analiza boje}{9}
\contentsline {subsection}{\numberline {3.1.3}Nedostaci pristupa}{10}
\contentsline {section}{\numberline {3.2}Implementacija konvolucijskom mre\IeC {\v z}om}{11}
\contentsline {subsection}{\numberline {3.2.1}Obrada ulaznog skupa podataka}{12}
\contentsline {subsection}{\numberline {3.2.2}Priprema konvolucijske mre\IeC {\v z}e}{12}
\contentsline {subsection}{\numberline {3.2.3}Arhitektura mre\IeC {\v z}e}{13}
\contentsline {subsubsection}{Ulazni dio}{13}
\contentsline {subsubsection}{Sredi\IeC {\v s}nji dio}{13}
\contentsline {subsubsection}{Izlazni dio}{13}
\contentsline {subsection}{\numberline {3.2.4}Treniranje mre\IeC {\v z}e}{14}
\contentsline {subsection}{\numberline {3.2.5}TensorBoard i proces treniranja}{15}
\contentsline {chapter}{\numberline {4}Rezultati implementiranog sustava}{16}
\contentsline {chapter}{\numberline {5}Kori\IeC {\v s}teni alati i tehnologije}{19}
\contentsline {section}{\numberline {5.1}Python}{19}
\contentsline {section}{\numberline {5.2}Tensorflow}{19}
\contentsline {section}{\numberline {5.3}OpenCV}{20}
\contentsline {chapter}{\numberline {6}Zaklju\IeC {\v c}ak}{21}
\contentsline {chapter}{Literatura}{22}
\contentsline {chapter}{Popis slika}{23}
\contentsline {chapter}{Popis tablica}{24}
