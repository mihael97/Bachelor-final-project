import cv2

IMAGE_SIZE = 80


def loadImageInGrayscale(path):
    image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    if image.data is None:
        return None
    resized = cv2.resize(image, (IMAGE_SIZE, IMAGE_SIZE))
    return resized.reshape(-1, IMAGE_SIZE, IMAGE_SIZE, 1)


def checkExtension(path):
    return path.endswith(".ppm") or path.endswith(".jpg") or path.endswith("png")
